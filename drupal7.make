core = 7.x
api = 2
projects[drupal][version] = 7.22

; Modules
projects[addressfield][version] = "1.0-beta3"

projects[admin_menu][version] = "3.0-rc4"

projects[advanced_help][version] = "1.0"

projects[audiofield][version] = "1.0-beta8"

projects[availability_calendars][version] = "4.1"

projects[service_links][version] = "2.1"

projects[backup_migrate][version] = "2.5"

projects[beautytips][version] = "2.0-beta2"

projects[better_exposed_filters][version] = "3.0-beta3"

projects[block_class][version] = "1.2"

projects[browscap][version] = "2.0"

projects[cacheexclude][version] = "2.3"

projects[calendar][version] = "3.4"

projects[cck][version] = "2.x-dev"

projects[ccl][version] = "1.5"

projects[ckeditor_link][version] = "2.3"

projects[ckeditor_link_file][version] = "1.2"

projects[clock][version] = "1.2"

projects[collapsiblock][version] = "1.0"

projects[colorbox}[version] = "2.3"

projects[commerce][version] = "1.6"

projects[context][version] = "3.0-beta6"

projects[cookiecontrol][version] = "1.6"

projects[cs_adaptive_image][version] = "1.0"

projects[css3pie][version] = "2.1"

projects[ctools][version] = "1.3"

projects[cufon][version] = "2.1"

projects[facetapi][version] = "1.3"

projects[flexslider][version] = "1.0-rc3"

projects[date][version] = "2.6"

projects[delta][version] = "3.0-beta11"

projects[devel][version] = "1.3"

projects[draggableviews][version] = "2.0"

projects[ds][version] = "2.2"

projects[fontyourface][version] = "2.7"

projects[ed_classified][version] = "3.0"

projects[elements][version] = "1.3"

projects[entity][version] = "1.0"

projects[entityreference][version] = "1.0"

projects[exclude_node_title][version] = "1.6"

projects[extlink][version] = "1.12"

projects[fb_social][version] = "2.0-beta4"

projects[features][version] = "1.0"

projects[field_collection][version] = "1.0-beta5"

projects[field_collection_table][version] = "1.0-beta1"

projects[field_group][version] = "1.1"

projects[fitvids][version] = "1.9"

projects[fivestar][version] = "2.0-alpha2"

projects[flippy][version] = "1.0"

projects[forward][version] = "1.4"

projects[galleryformatter][version] = "1.3"

projects[geocoder][version] = "1.2"

projects[geofield][version] = "1.1"

projects[geophp][version] = "1.7"

projects[globalredirect][version] = "1.5"

projects[google_analytics][version] = "1.3"

projects[guestbook][version] = "2.x-dev"

projects[fences][version] = "1.0"

projects[html5_tools][version] = "1.2"

projects[i18n][version] = "1.8"

projects[i18n_contrib][version] = "1.0-alpha1"

projects[imagecache_actions][version] = "1.1"

projects[imce][version] = "1.7"

projects[imce_wysiwyg][version] = "1.0"

projects[jplayer][version] = "2.0-beta1"

projects[languageicons][version] = "1.0"

projects[libraries][version] = "2.1"

projects[link][version] = "1.1"

projects[linkit][version] = "2.6"

projects[logintoboggan][version] = "1.3"

projects[mailchimp][version] = "2.10"

projects[mailsystem][version] = "2.34"

projects[masquerade][version] = "1.0-rc5"

projects[media][version] = "1.2"

projects [media_youtube][version] = "2.0-rc1"

projects [media_vimeo] = "1.0-beta5"

projects[menu_firstchild][version] = "1.1"

projects[menuux][version] = "1.0-beta3"

projects[metatag][version] = "1.0-beta6"

projects[migrate][version] = "2.5"

projects[migrate_extras][version] = "2.5"

projects[mimemail][version] = "1.0-alpha2"

projects[module_filter][version] = "1.7"

projects[mollom][version] = "2.5"

projects[mytinytodo][version] = "1.0"

projects[nice_menus][version] = "2.1"

projects[node_export][version] = "3.0"

projects[oauth][version] = "3.1"

projects[omega_tools][version] = "3.0-rc4"

projects[openlayers][version] = "2.0-beta5"

projects[page_title][version] = "2.7"

projects[panels][version] = "3.3"

projects[pathauto][version] = "1.2"

projects[pathologic][version] = "2.10"

projects[persistent_login][version] = "1.0-beta1"

projects[plup][version] = "1.0-alpha1"

projects[privatemsg][version] = "1.3"

projects[profile2][version] = "1.3"

projects[quicktabs][version] = "3.4"

projects[redirect][version] = "1.0-rc1"

projects[resp_img][version] = "1.3"

projects[ro_cookie_opt_in][version] = "1.5"

projects[rooms][version] = "1.0"

projects[rules][version] = "2.3"

projects[scheduler][version] = "1.0"

projects[schemaorg][version] = "1.0-beta3"

projects[search_api][version] = "1.4"

projects[search_api_db][version] = "1.0-beta4"

projects[select_or_other][version] = "2.15"

projects[simple_mobile_redirect][version] = "1.1"

projects[simplenews][version] = "1.0"

projects[site_map][version] = "1.0"

projects[site_verify][version] = "1.0"

projects[smtp][version] = "1.0"

projects[socialmedia][version] = "1.0-beta13"

projects[starrating][version] = "1.0"

projects[storage_api][version] = "1.5"

projects[superfish][version] = "1.x-dev"

projects[token][version] = "1.5"

projects[transliteration][version] = "3.1"

projects[twitter][version] = "5.6"

projects[uuid][version] = "1.0-alpha3"

projects[variable][version] = "2.2"

projects[videojs][version] = "2.2"

projects[views][version] = "3.7"

projects[views_accordion][version] = "1.0-rc2"

projects[views_export_xls][version] = "1.0"

projects[views_slideshow][version] = "3.0"

projects[votingapi][version] = "2.11"

projects[webform][version] = "3.18"

projects[webform_addmore][version] = "1.01"

projects[webform_conditional][version] = "1.0-beta1"

projects[webform_mysql_views][version] = "1.0"

projects[widgets][version] = "1.0-rc1"

projects[wysiwyg][version] = "2.2"

projects[xmlsitemap][version] = "2.0-rc2"


; Features =====================================================================

projects[Responsive-imagestyles][type] = module
projects[Responsive-imagestyles][download][type] = git
projects[Responsive-imagestyles][download][url] = git://github.com/webfed/Responsive-imagestyles.git
projects[Responsive-imagestyles][download][branch] = master
projects[Responsive-imagestyles][subdir] = custom/features

projects[breakpoints_responsive_images][type] = module
projects[breakpoints_responsive_images][download][type] = git
projects[breakpoints_responsive_images][download][url] = git://github.com/webfed/breakpoints_responsive_images.git
projects[breakpoints_responsive_images][download][branch] = master
projects[breakpoints_responsive_images][subdir] = custom/features





; Patches
projects[rules][patch][] = "http://drupal.org/files/rules.state_.inc-ajax-error.patch"

projects [fontyourface][patch][] = "http://drupal.org/files/fontyourface-error-connecting-fonts-com-1923604-10.patch"

;Todo
;Error: Unable to patch smtp with smtp-888856-2_0.patch.
;projects[smtp][patch][] = "http://drupal.org/files/issues/smtp-888856-2_0.patch"


; Themes
projects[sasson][version] = "2.9"

projects[acquia_marina][version] = "2.0-beta1"

projects[adaptivetheme][version] = "3.1"

projects[omega][version] = "3.1"

projects[corolla][version] = "3.0-rc1"

projects[hotel][version] = "1.x-dev"

;projects[nucleus][version] = "1.2"

projects[zen][version] = "5.1"


; Libraries

libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.6.1/ckeditor_3.6.6.1.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"

libraries[colorbox][download][type] = "git"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox.git"
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][type] = "library"

libraries[cufon][download][type] = "get"
libraries[cufon][download][url] = "http://cdnjs.cloudflare.com/ajax/libs/cufon/1.09i/cufon-yui.js"
libraries[cufon][directory_name] = "cufon"
libraries[cufon][type] = "library"

libraries[fitvids][download][type] = "git"
libraries[fitvids][download][url] = "https://github.com/davatron5000/FitVids.js.git"
libraries[fitvids][directory_name] = "fitvids"
libraries[fitvids][type] = "library"

libraries[flexslider][download][type] = "git"
libraries[flexslider][download][url] = "https://github.com/woothemes/FlexSlider.git"
libraries[flexslider][directory_name] = "flexslider"
libraries[flexslider][type] = "library"

libraries[ckeditor][download][type] = ""
libraries[ckeditor][download][url] = ""
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"

libraries[geoPHP][download][type] = "get"
libraries[geoPHP][download][url] = "https://github.com/downloads/phayes/geoPHP/geoPHP.tar.gz"
libraries[geoPHP][directory_name] = "geoPHP"
libraries[geoPHP][type] = "library"

libraries[jplayer][download][type] = "git"
libraries[jplayer][download][url] = "https://github.com/happyworm/jPlayer.git"
libraries[jplayer][directory_name] = "jplayer"
libraries[jplayer][type] = "library"

libraries[jquery.cycle][download][type] = "get"
libraries[jquery.cycle][download][url] = "http://malsup.github.com/jquery.cycle.all.js"
libraries[jquery.cycle][directory_name] = "jquery.cycle"
libraries[jquery.cycle][type] = "library"

libraries[json2][download][type] = "git"
libraries[json2][download][url] = "https://github.com/douglascrockford/JSON-js.git"
libraries[json2][directory_name] = "json2"
libraries[json2][type] = "library"

libraries[mailchimp][download][type] = "get"
libraries[mailchimp][download][url] = "http://apidocs.mailchimp.com/api/downloads/mailchimp-api-class.zip"
libraries[mailchimp][directory_name] = "mailchimp"
libraries[mailchimp][type] = "library"

libraries[OpenLayers-2.12][download][type] = "get"
libraries[OpenLayers-2.12][download][url] = "http://openlayers.org/download/OpenLayers-2.12.tar.gz"
libraries[OpenLayers-2.12][directory_name] = "OpenLayers-2.12"
libraries[OpenLayers-2.12][type] = "library"

libraries[PIE][download][type] = "git"
libraries[PIE][download][url] = "https://github.com/lojjic/PIE.git"
libraries[PIE][directory_name] = "PIE"
libraries[PIE][type] = "library"

libraries[plupload][download][type] = "get"
libraries[plupload][download][url] = "http://plupload.com/downloads/plupload_1_5_6.zip"
libraries[plupload][directory_name] = "plupload"
libraries[plupload][type] = "library"

libraries[rijksoverheid-cookie-opt-in][download][type] = "get"
libraries[rijksoverheid-cookie-opt-in][download][url] = "http://www.rijksoverheid.nl/bestanden/documenten-en-publicaties/brochures/2012/11/22/download-rijksoverheid-cookie-opt-in-zip/rijksoverheid-cookie-opt-in-v1-1.zip"
libraries[rijksoverheid-cookie-opt-in][directory_name] = "rijksoverheid-cookie-opt-in"
libraries[rijksoverheid-cookie-opt-in][type] = "library"

libraries[superfish][download][type] = "git"
libraries[superfish][download][url] = "https://github.com/mehrpadin/Superfish-for-Drupal.git"
libraries[superfish][directory_name] = "superfish"
libraries[superfish][type] = "library"

libraries[timepicker][download][type] = "git"
libraries[timepicker][download][url] = "https://github.com/wvega/timepicker.git"
libraries[timepicker][directory_name] = "wvega-timepicker"
libraries[timepicker][type] = "library"